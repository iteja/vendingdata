<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ventas; 
use app\models\MaquinasVending;
use app\models\PiezasReemplazadas;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */

public function actionIndex()
{
    // Obtén la cantidad de máquinas en cada estado
    $estadosMaquinas = MaquinasVending::find()
        ->select(['estado', 'COUNT(*) AS cantidad'])
        ->groupBy(['estado'])
        ->asArray()
        ->all();

    // Formatea los datos para el gráfico Highcharts de máquinas
    $estados = [];
    $cantidadesMaquinas = [];
    foreach ($estadosMaquinas as $estado) {
        $estados[] = $estado['estado'];
        $cantidadesMaquinas[] = (int) $estado['cantidad'];
    }

    // Obtén los datos de la tabla ventas
    $ventasData = Ventas::find()
        ->select(['DATE_FORMAT(fecha_hora, "%Y-%m") AS mes_anio', 'COUNT(idProductos) AS totalVentas'])
        ->groupBy(['mes_anio'])
        ->orderBy(['mes_anio' => SORT_DESC])
        ->limit(10)
        ->asArray()
        ->all();

    // Formatea los datos para el gráfico Highcharts de ventas
    $fechas = [];
    $cantidadesVentas = [];
    foreach ($ventasData as $venta) {
        $fechas[] = $venta['mes_anio']; 
        $cantidadesVentas[] = (int) $venta['totalVentas'];
    }
    
    $piezasReemplazadasData = PiezasReemplazadas::find()
        ->select(['piezas_reemplazadas', 'COUNT(*) AS cantidad'])
        ->groupBy(['piezas_reemplazadas'])
        ->orderBy(['cantidad' => SORT_DESC])
        ->limit(10)
        ->asArray()
        ->all();

    // Formatea los datos para el gráfico Highcharts de piezas reemplazadas
    $piezasReemplazadas = [];
    $cantidadesPiezasReemplazadas = [];
    foreach ($piezasReemplazadasData as $pieza) {
        $piezasReemplazadas[] = $pieza['piezas_reemplazadas'];
        $cantidadesPiezasReemplazadas[] = (int) $pieza['cantidad'];
    }

    return $this->render('index', [
        'fechas' => $fechas,
        'montos' => $cantidadesVentas,
        'estados' => $estados,
        'cantidades' => $cantidadesMaquinas,
        'piezasReemplazadas' => $piezasReemplazadas,
        'cantidadesPiezasReemplazadas' => $cantidadesPiezasReemplazadas,
    ]);
}


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
   

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
