<?php

namespace app\controllers;

use app\models\Piezasreemplazadas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PiezasreemplazadasController implements the CRUD actions for Piezasreemplazadas model.
 */
class PiezasreemplazadasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Piezasreemplazadas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Piezasreemplazadas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idAverias' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Piezasreemplazadas model.
     * @param int $idAverias Id Averias
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idAverias)
    {
        return $this->render('view', [
            'model' => $this->findModel($idAverias),
        ]);
    }

    /**
     * Creates a new Piezasreemplazadas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Piezasreemplazadas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idAverias' => $model->idAverias]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Piezasreemplazadas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idAverias Id Averias
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idAverias)
    {
        $model = $this->findModel($idAverias);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idAverias' => $model->idAverias]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Piezasreemplazadas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idAverias Id Averias
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idAverias)
    {
        $this->findModel($idAverias)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Piezasreemplazadas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idAverias Id Averias
     * @return Piezasreemplazadas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idAverias)
    {
        if (($model = Piezasreemplazadas::findOne(['idAverias' => $idAverias])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
