<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "averias".
 *
 * @property int $id
 * @property int|null $idMaquinasVending
 * @property string|null $descripcion
 * @property string|null $prioridad_averia
 * @property string|null $fecha_registro
 *
 * @property MaquinasVending $idMaquinasVending0
 * @property PiezasReemplazadas $piezasReemplazadas
 */
class Averias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'averias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idMaquinasVending'], 'integer'],
            [['fecha_registro'], 'safe'],
            [['descripcion'], 'string', 'max' => 255],
            [['prioridad_averia'], 'string', 'max' => 100],
            [['idMaquinasVending'], 'exist', 'skipOnError' => true, 'targetClass' => MaquinasVending::class, 'targetAttribute' => ['idMaquinasVending' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idMaquinasVending' => 'Id Maquinas Vending',
            'descripcion' => 'Descripcion',
            'prioridad_averia' => 'Prioridad Averia',
            'fecha_registro' => 'Fecha Registro',
        ];
    }

    /**
     * Gets query for [[IdMaquinasVending0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMaquinasVending0()
    {
        return $this->hasOne(MaquinasVending::class, ['id' => 'idMaquinasVending']);
    }

    /**
     * Gets query for [[PiezasReemplazadas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPiezasReemplazadas()
    {
        return $this->hasOne(PiezasReemplazadas::class, ['idAverias' => 'id']);
    }
}
