<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tarifas".
 *
 * @property int $id
 * @property int|null $idProductos
 * @property float|null $precio_programado
 * @property string|null $fecha_vigencia
 *
 * @property Productos $idProductos0
 */
class Tarifas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tarifas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idProductos'], 'integer'],
            [['precio_programado'], 'number'],
            [['fecha_vigencia'], 'safe'],
            [['idProductos'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::class, 'targetAttribute' => ['idProductos' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idProductos' => 'Id Productos',
            'precio_programado' => 'Precio Programado',
            'fecha_vigencia' => 'Fecha Vigencia',
        ];
    }

    /**
     * Gets query for [[IdProductos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProductos0()
    {
        return $this->hasOne(Productos::class, ['id' => 'idProductos']);
    }
}
