<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mantenimientos".
 *
 * @property int $id
 * @property int|null $idMaquinasVending
 * @property string|null $descripcion
 * @property string|null $estado
 * @property string|null $fecha_registro
 *
 * @property MaquinasVending $idMaquinasVending0
 */
class Mantenimientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mantenimientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idMaquinasVending'], 'integer'],
            [['fecha_registro'], 'safe'],
            [['descripcion'], 'string', 'max' => 255],
            [['estado'], 'string', 'max' => 100],
            [['idMaquinasVending'], 'exist', 'skipOnError' => true, 'targetClass' => MaquinasVending::class, 'targetAttribute' => ['idMaquinasVending' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idMaquinasVending' => 'Id Maquinas Vending',
            'descripcion' => 'Descripcion',
            'estado' => 'Estado',
            'fecha_registro' => 'Fecha Registro',
        ];
    }

    /**
     * Gets query for [[IdMaquinasVending0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMaquinasVending0()
    {
        return $this->hasOne(MaquinasVending::class, ['id' => 'idMaquinasVending']);
    }
}
