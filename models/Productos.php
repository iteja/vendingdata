<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $id
 * @property int|null $idMaquinasVending
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property int|null $duracion_anios
 * @property int|null $duracion_meses
 * @property int|null $duracion_dias
 * @property float|null $precio_compra
 * @property float|null $precio_venta
 *
 * @property MaquinasVending $idMaquinasVending0
 * @property Tarifas[] $tarifas
 * @property Ventas[] $ventas
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idMaquinasVending', 'duracion_anios', 'duracion_meses', 'duracion_dias'], 'integer'],
            [['precio_compra', 'precio_venta'], 'number'],
            [['nombre'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 255],
            [['idMaquinasVending'], 'exist', 'skipOnError' => true, 'targetClass' => MaquinasVending::class, 'targetAttribute' => ['idMaquinasVending' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idMaquinasVending' => 'Id Maquinas Vending',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'duracion_anios' => 'Duracion Anios',
            'duracion_meses' => 'Duracion Meses',
            'duracion_dias' => 'Duracion Dias',
            'precio_compra' => 'Precio Compra',
            'precio_venta' => 'Precio Venta',
        ];
    }

    /**
     * Gets query for [[IdMaquinasVending0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMaquinasVending0()
    {
        return $this->hasOne(MaquinasVending::class, ['id' => 'idMaquinasVending']);
    }

    /**
     * Gets query for [[Tarifas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTarifas()
    {
        return $this->hasMany(Tarifas::class, ['idProductos' => 'id']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::class, ['idProductos' => 'id']);
    }
}
