<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "piezas_reemplazadas".
 *
 * @property int $idAverias
 * @property string|null $piezas_reemplazadas
 *
 * @property Averias $idAverias0
 */
class PiezasReemplazadas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piezas_reemplazadas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idAverias'], 'required'],
            [['idAverias'], 'integer'],
            [['piezas_reemplazadas'], 'string', 'max' => 255],
            [['idAverias'], 'unique'],
            [['idAverias'], 'exist', 'skipOnError' => true, 'targetClass' => Averias::class, 'targetAttribute' => ['idAverias' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idAverias' => 'Id Averias',
            'piezas_reemplazadas' => 'Piezas Reemplazadas',
        ];
    }

    /**
     * Gets query for [[IdAverias0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAverias0()
    {
        return $this->hasOne(Averias::class, ['id' => 'idAverias']);
    }
}
