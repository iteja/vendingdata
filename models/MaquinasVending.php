<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "maquinas_vending".
 *
 * @property int $id
 * @property int|null $idLocalizaciones
 * @property string|null $tipo_maquina
 * @property string|null $modelo
 * @property string|null $estado
 * @property string|null $fecha_instalacion
 *
 * @property Averias[] $averias
 * @property Localizaciones $idLocalizaciones0
 * @property Mantenimientos[] $mantenimientos
 * @property Productos[] $productos
 */
class MaquinasVending extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'maquinas_vending';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idLocalizaciones'], 'integer'],
            [['fecha_instalacion'], 'safe'],
            [['tipo_maquina', 'modelo', 'estado'], 'string', 'max' => 100],
            [['idLocalizaciones'], 'exist', 'skipOnError' => true, 'targetClass' => Localizaciones::class, 'targetAttribute' => ['idLocalizaciones' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idLocalizaciones' => 'Id Localizaciones',
            'tipo_maquina' => 'Tipo Maquina',
            'modelo' => 'Modelo',
            'estado' => 'Estado',
            'fecha_instalacion' => 'Fecha Instalacion',
        ];
    }

    /**
     * Gets query for [[Averias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAverias()
    {
        return $this->hasMany(Averias::class, ['idMaquinasVending' => 'id']);
    }

    /**
     * Gets query for [[IdLocalizaciones0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdLocalizaciones0()
    {
        return $this->hasOne(Localizaciones::class, ['id' => 'idLocalizaciones']);
    }

    /**
     * Gets query for [[Mantenimientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMantenimientos()
    {
        return $this->hasMany(Mantenimientos::class, ['idMaquinasVending' => 'id']);
    }

    /**
     * Gets query for [[Productos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Productos::class, ['idMaquinasVending' => 'id']);
    }
}
