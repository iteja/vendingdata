<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "localizaciones".
 *
 * @property int $id
 * @property int|null $idRutas
 * @property string|null $provincia
 * @property string|null $ciudad
 * @property string|null $calle_numero
 * @property string|null $coordenadas
 * @property string|null $fecha_hora_actualizacion
 *
 * @property Rutas $idRutas0
 * @property MaquinasVending[] $maquinasVendings
 */
class Localizaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'localizaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idRutas'], 'integer'],
            [['fecha_hora_actualizacion'], 'safe'],
            [['provincia', 'ciudad', 'calle_numero', 'coordenadas'], 'string', 'max' => 100],
            [['idRutas'], 'exist', 'skipOnError' => true, 'targetClass' => Rutas::class, 'targetAttribute' => ['idRutas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idRutas' => 'Id Rutas',
            'provincia' => 'Provincia',
            'ciudad' => 'Ciudad',
            'calle_numero' => 'Calle Numero',
            'coordenadas' => 'Coordenadas',
            'fecha_hora_actualizacion' => 'Fecha Hora Actualizacion',
        ];
    }

    /**
     * Gets query for [[IdRutas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdRutas0()
    {
        return $this->hasOne(Rutas::class, ['id' => 'idRutas']);
    }

    /**
     * Gets query for [[MaquinasVendings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaquinasVendings()
    {
        return $this->hasMany(MaquinasVending::class, ['idLocalizaciones' => 'id']);
    }
}
