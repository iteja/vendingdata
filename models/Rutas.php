<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rutas".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $descripcion
 *
 * @property Localizaciones[] $localizaciones
 */
class Rutas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rutas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Localizaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLocalizaciones()
    {
        return $this->hasMany(Localizaciones::class, ['idRutas' => 'id']);
    }
}
