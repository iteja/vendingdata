<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ventas".
 *
 * @property int $id
 * @property int|null $idProductos
 * @property string|null $fecha_hora
 *
 * @property Productos $idProductos0
 */
class Ventas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idProductos'], 'integer'],
            [['fecha_hora'], 'safe'],
            [['idProductos'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::class, 'targetAttribute' => ['idProductos' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idProductos' => 'Id Productos',
            'fecha_hora' => 'Fecha Hora',
        ];
    }

    /**
     * Gets query for [[IdProductos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProductos0()
    {
        return $this->hasOne(Productos::class, ['id' => 'idProductos']);
    }
}
