<?php
use dmstr\widgets\Alert;
use miloschuman\highcharts\Highcharts;

$this->title = 'VendControl';

?>

<section class="content-header">
    <h1>
        Dashboard
        <small>Panel de control</small>
    </h1>
    <br>
</section>

<section class="content">
    <?= Alert::widget() ?>

    <div class="row">
        <div class="col-md-6">
            <!-- Resumen de Estadísticas y Gráficos -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Estadísticas de Ventas</h3>
                </div>
                <div class="box-body">
                    <?php
                    echo Highcharts::widget([
                        'options' => [
                            'chart' => [
                                'type' => 'column', // Tipo de gráfico (columna)
                            ],
                            'title' => [
                                'text' => 'Ventas Mensuales',
                            ],
                            'xAxis' => [
                                'categories' => $fechas,
                                'title' => [
                                    'text' => 'Meses',
                                ],
                            ],
                            'yAxis' => [
                                'title' => [
                                    'text' => 'Monto de Ventas',
                                ],
                            ],
                            'plotOptions' => [
                                'column' => [
                                    'dataLabels' => [
                                        'enabled' => true, // Muestra etiquetas en las columnas
                                    ],
                                ],
                            ],
                            'series' => [
                                [
                                    'name' => 'Ventas',
                                    'data' => $montos,
                                    'color' => '#3498db', // Color de las columnas
                                ],
                            ],
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <!-- Gráfico de Estados de Máquinas -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Estados de Máquinas</h3>
                </div>
                <div class="box-body">
                    <?php
                    echo Highcharts::widget([
                        'options' => [
                            'chart' => [
                                'type' => 'pie', // Tipo de gráfico (pie)
                            ],
                            'title' => ['text' => 'Estados de Máquinas'],
                            'series' => [
                                [
                                    'name' => 'Máquinas',
                                    'data' => array_map(function ($estado, $cantidad) {
                                        return ['name' => $estado, 'y' => $cantidad];
                                    }, $estados, $cantidades),
                                ],
                            ],
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <!-- Gráfico de Piezas más Reemplazadas (Diagrama Horizontal) -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Piezas más Reemplazadas</h3>
                </div>
                <div class="box-body">
                    <?php
                    echo Highcharts::widget([
                        'options' => [
                            'chart' => [
                                'type' => 'bar', // Tipo de gráfico (barras)
                            ],
                            'title' => [
                                'text' => 'Piezas más Reemplazadas',
                            ],
                            'xAxis' => [
                                'categories' => $piezasReemplazadas,
                                'title' => [
                                    'text' => 'Cantidad de Reemplazos',
                                ],
                            ],
                            'yAxis' => [
                                'title' => [
                                    'text' => 'Piezas',
                                ],
                            ],
                            'plotOptions' => [
                                'bar' => [
                                    'dataLabels' => [
                                        'enabled' => true, // Muestra etiquetas en las barras
                                    ],
                                ],
                            ],
                            'series' => [
                                [
                                    'name' => 'Reemplazos',
                                    'data' => $cantidadesPiezasReemplazadas,
                                    'color' => '#e74c3c', // Color de las barras
                                ],
                            ],
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

</section>
