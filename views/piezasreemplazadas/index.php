<?php

use app\models\Piezasreemplazadas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Piezasreemplazadas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="piezasreemplazadas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Piezasreemplazadas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idAverias',
            'piezas_reemplazadas',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Piezasreemplazadas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idAverias' => $model->idAverias]);
                 }
            ],
        ],
    ]); ?>


</div>
