<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Piezasreemplazadas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="piezasreemplazadas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idAverias')->textInput() ?>

    <?= $form->field($model, 'piezas_reemplazadas')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
