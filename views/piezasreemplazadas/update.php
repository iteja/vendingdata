<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Piezasreemplazadas $model */

$this->title = 'Update Piezasreemplazadas: ' . $model->idAverias;
$this->params['breadcrumbs'][] = ['label' => 'Piezasreemplazadas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idAverias, 'url' => ['view', 'idAverias' => $model->idAverias]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="piezasreemplazadas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
