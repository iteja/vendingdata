<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Piezasreemplazadas $model */

$this->title = 'Create Piezasreemplazadas';
$this->params['breadcrumbs'][] = ['label' => 'Piezasreemplazadas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="piezasreemplazadas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
