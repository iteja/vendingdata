<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Piezasreemplazadas $model */

$this->title = $model->idAverias;
$this->params['breadcrumbs'][] = ['label' => 'Piezasreemplazadas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="piezasreemplazadas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idAverias' => $model->idAverias], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idAverias' => $model->idAverias], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idAverias',
            'piezas_reemplazadas',
        ],
    ]) ?>

</div>
