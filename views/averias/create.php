<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Averias $model */

$this->title = 'Create Averias';
$this->params['breadcrumbs'][] = ['label' => 'Averias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="averias-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
