<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Averias $model */

$this->title = 'Update Averias: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Averias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="averias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
