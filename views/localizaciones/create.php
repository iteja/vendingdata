<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Localizaciones $model */

$this->title = 'Create Localizaciones';
$this->params['breadcrumbs'][] = ['label' => 'Localizaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="localizaciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
