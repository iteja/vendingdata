<?php

use app\models\Localizaciones;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Localizaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="localizaciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Localizaciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'idRutas',
            'provincia',
            'ciudad',
            'calle_numero',
            //'coordenadas',
            //'fecha_hora_actualizacion',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Localizaciones $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
