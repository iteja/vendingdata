<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Localizaciones $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="localizaciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idRutas')->textInput() ?>

    <?= $form->field($model, 'provincia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ciudad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'calle_numero')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'coordenadas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_hora_actualizacion')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
