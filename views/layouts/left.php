<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/default-profile.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Iker Teja</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'CRUDS', 'options' => ['class' => 'header']],
                    ['label' => 'Averias', 'url' => ['/averias/index']],
                    ['label' => 'Localizaciones', 'url' => ['/localizaciones/index']],
                    ['label' => 'Mantenimientos', 'url' => ['/mantenimientos/index']],
                    ['label' => 'Maquinas Vending', 'url' => ['/maquinasvending/index']],
                    ['label' => 'Piezas Reemplazadas', 'url' => ['/piezasreemplazadas/index']],
                    ['label' => 'Productos', 'url' => ['/productos/index']],
                    ['label' => 'Rutas', 'url' => ['/rutas/index']],
                    ['label' => 'Tarifas', 'url' => ['/tarifas/index']],
                    ['label' => 'Ventas', 'url' => ['/ventas/index']],
                ],
            ]
        ) ?>

    </section>

</aside>
