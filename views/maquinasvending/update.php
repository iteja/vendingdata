<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Maquinasvending $model */

$this->title = 'Update Maquinasvending: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Maquinasvendings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="maquinasvending-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
