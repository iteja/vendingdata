<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Maquinasvending $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="maquinasvending-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idLocalizaciones')->textInput() ?>

    <?= $form->field($model, 'tipo_maquina')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'modelo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estado')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_instalacion')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
