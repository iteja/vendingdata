<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Maquinasvending $model */

$this->title = 'Create Maquinasvending';
$this->params['breadcrumbs'][] = ['label' => 'Maquinasvendings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maquinasvending-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
