<?php

use app\models\Maquinasvending;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Maquinasvending';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maquinasvending-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Maquinasvending', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'idLocalizaciones',
            'tipo_maquina',
            'modelo',
            'estado',
            //'fecha_instalacion',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Maquinasvending $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
