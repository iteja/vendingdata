<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Tarifas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="tarifas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idProductos')->textInput() ?>

    <?= $form->field($model, 'precio_programado')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_vigencia')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
